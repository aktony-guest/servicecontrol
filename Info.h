#ifndef _CHK_H
#define _CHK_H

#define VERSION "1.3"

#define ABOUT_INFO "\n\
 ServiceControl 1.3                                                        \n\
                                                                           \n\
 Navigation keys:                                                          \n\
           Up    - move cursor up    | Down   - move cursor down           \n\
           right - move cursor right | left   - move cursor left           \n\
           PgUp  - move page up      | PgDown - move page down             \n\
           /     - for search        | ?      - for this help              \n\
 Action keys:                                                              \n\
           Space - anwählen/abwählen | F10 - exit                          \n\
           F1    - start/stop unit   | F2  - reload/update                 \n\
           F3    - enabled unit      | F4  - disabled unit                 \n\
           F5    - unmasked unit     | F6  - masked unit                   \n\
---------------------------------------------------------------------------\n\
                     License: GPLv3 (c) Anthony Olszynski                  \n\
"

#endif
