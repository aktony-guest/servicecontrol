TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt


unix:!macx: LIBS += -Wall -lncursesw -lmenuw -lformw -lpanelw -pthread -ldbus-c++-1 -lsystemd

SOURCES += \
        MenuClass.cpp \
        ServiceClass.cpp \
        Start.cpp \
        TaskClass.cpp \
        WinClass.cpp

HEADERS += \
    Info.h \
    MenuClass.h \
    ServiceClass.h \
    TaskClass.h \
    WinClass.h
